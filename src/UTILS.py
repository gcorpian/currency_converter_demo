#---------- IMPORTS ----------
import os
import sys
import time
import smtplib
import base64
import subprocess
#import wmi
#import pyscreenshot as ImageGrab# pip install
from datetime import datetime

#-------CUSTOM IMPORTS -------
import CONFIG

#---------- VARIABLE DECLARATIONS ----------
indent = CONFIG.indent

def ReportErrors(msg, record):
    if record != "":
        CONFIG.errorList.append([record["row"], record["description"], msg])

# This will print a summary of any errors found
def ErrorSummary():
    CONFIG.testSummary = "\n" + "ERROR SUMMARY\n"
    
    if len(CONFIG.errorList) == 1:
        CONFIG.testSummary = CONFIG.testSummary + indent + "There was 1 error encountered.\n"
    else:
        CONFIG.testSummary = CONFIG.testSummary + indent + "There were " + str(len(CONFIG.errorList)) + " errors encountered.\n"
    
    for error in CONFIG.errorList:
        CONFIG.testSummary = CONFIG.testSummary + indent + "Row: " + str(error[0]) + ", Description: " + error[1] + ":\n"
        CONFIG.testSummary = CONFIG.testSummary + indent * 2 + "Error: " + error[2] + "\n"

    CONFIG.logger1.info(CONFIG.testSummary)
    