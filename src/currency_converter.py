#---------- IMPORTS ----------
from datetime import datetime
from getpass import getuser
import os
import sys
import time
import xlrd
import traceback
from subprocess import check_output
from tkinter import Tk, messagebox, Button

#-------CUSTOM IMPORTS -------
import CONFIG
import API
import UTILS

#---------- VARIABLE DECLARATIONS ----------
indent = CONFIG.indent
counter0, APIResponse, reqStatus = ("",) * 3
record, SSData = ([],) * 2

# look for command line arguments being passed in for spreadsheet path and worksheet name
try:
    if sys.argv[1] and sys.argv[2]:
        CONFIG.filename = sys.argv[1]
        CONFIG.worksheet = sys.argv[2]
except:
    pass

# This will connect to the Excel spreadsheet and parse the data into a list of dictionaries
def GetData(filename, worksheet):
    #global SSData
    #check filename exists
    if os.path.isfile(filename):
        workbook = xlrd.open_workbook(filename)
        sheet = workbook.sheet_by_name(worksheet)

        curr_row = 0
        num_rows = sheet.nrows - 1
        while curr_row < num_rows:
            curr_row += 1
            #Assign values to test parameters if the row is to be run
            strAuto = sheet.cell_value(curr_row, 0)
            if strAuto.upper() == "":
                break
            strTestID = sheet.cell_value(curr_row, 1)
            strDescription = sheet.cell_value(curr_row, 2)
            strTask = sheet.cell_value(curr_row, 3).upper()
            strFromCurrency = sheet.cell_value(curr_row, 4).upper()
            if strFromCurrency == "":
                strFromCurrency = "null"
            strToCurrency = sheet.cell_value(curr_row, 5).upper()
            if strToCurrency == "":
                strToCurrency = "null"
            strAmount = str(sheet.cell_value(curr_row, 6))
            strDate = str(sheet.cell_value(curr_row, 7))
            strExpectedResults = str(sheet.cell_value(curr_row, 8))
            strComments = sheet.cell_value(curr_row, 9)

            SSData.append({"row":curr_row+1, "auto":strAuto, "id":strTestID, "description":strDescription, "Task":strTask, "FromCurrency":strFromCurrency, "ToCurrency":strToCurrency, "Amount":strAmount, "Date":strDate, "results":strExpectedResults, "comments":strComments})
        return SSData
    else:
        UTILS.ReportErrors("The Test Spreadsheet does not exist in location: " + filename, "")
        sys.exit()

def CreateMessageBox(format, title, message):
    top = Tk()
    if format == "askyesno":
        result = messagebox.askyesno(title, message)
    button = Button(top, text="Close Me", command=top.destroy)
    button.pack()

    return result

def main():
    start = datetime.strptime(str(datetime.now().time()), "%H:%M:%S.%f")

    CONFIG.logger1.info("Worksheet: " + CONFIG.worksheet + " in " + CONFIG.filename[CONFIG.filename.rfind("/")+1:])
    CONFIG.logger1.info("System Host: " + check_output("hostname").decode("ascii").strip())
    CONFIG.logger1.info("Test User: " + getuser())
    CONFIG.logger1.info("Start time: " + str(datetime.now().time()) + "\n")
    
    try:
        SSData = GetData(CONFIG.filename, CONFIG.worksheet)

        for record in SSData:
            if record["auto"] == "B":
                CONFIG.logger1.info("\nA breakpoint was encountered in the spreadsheet at row " + str(record["row"]))
                #result = CreateMessageBox("askyesno", "Breakpoint Found", "A breakpoint has been encountered in your test at row " + str(record["row"]) + ".  Do you wish to Continue?")
                result = messagebox.askquestion('Breakpoint Found', "A breakpoint has been encountered in your test at row " + str(record["row"]) + ".  Do you wish to Continue?")
                if result in [False, "no"]:
                    CONFIG.logger1.info("\nEnding Test")
                    sys.exit()
                else:
                    CONFIG.logger1.info("\nContinuing Test")
                    record["auto"] = "Y"
    
            if record["auto"] == "Y":
                if record["row"] > 1: CONFIG.logger1.info("")                
                CONFIG.logger1.info("Worksheet: " + CONFIG.worksheet + "   Row: " + str(record["row"]) + "   Description: " + record["description"])

                API.ConvertCurrencyAPI(record)
                time.sleep(2)
                continue
            if record["auto"] == "":                    
                CONFIG.logger1.info("End of test iteration.")
                break
        
        UTILS.ErrorSummary()

        stop = datetime.strptime(str(datetime.now().time()), "%H:%M:%S.%f")
        CONFIG.logger1.info("Stop time: " + str(stop))
        
        #CONFIG.logger1.info("Completion Time: %s" % str(stop))
        tDelta = stop - start
        CONFIG.testDuration = "Test Duration: %s" % str(tDelta)
        CONFIG.logger1.info(CONFIG.testDuration)
        
    except SystemExit as e:
        sys.exit(e)
    except:
        if str(sys.exc_info()[1]) == "invalid server address":
            #print("EXCEPTION: " + str(sys.exc_info()[1]))
            print("Ending Test")
        else:
            UTILS.ReportErrors(str(sys.exc_info()), record)
            UTILS.ReportErrors(str(traceback.format_exc()), record) 
            CONFIG.logger1.info("Stop time: " + str(datetime.now().time()))
            stop = datetime.strptime(str(datetime.now().time()), "%H:%M:%S.%f")
            tDelta = stop - start
            CONFIG.testDuration = "Test Duration: %s\n" % str(tDelta)
            CONFIG.CloseLogFile()
            time.sleep(5)
        
            sys.exit()
    
    CONFIG.CloseLogFile()
    
if __name__ == "__main__":
    main()