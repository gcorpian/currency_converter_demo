#---------- IMPORTS ----------
import requests
import json
#import time
#from subprocess import Popen, PIPE, STDOUT

#-------CUSTOM IMPORTS -------
import CONFIG
import UTILS

#---------- VARIABLE DECLARATIONS ----------
indent = CONFIG.indent
APIResponse = ""
reqStatus = ""

def extract_values(obj, key):
    """Pull all values of specified key from nested JSON."""
    arr = []

    def extract(obj, arr, key):
        """Recursively search for values of key in JSON tree."""
        if isinstance(obj, dict):
            for k, v in obj.items():
                if isinstance(v, (dict, list)):
                    extract(v, arr, key)
                elif k == key:
                    arr.append(v)
        elif isinstance(obj, list):
            for item in obj:
                extract(item, arr, key)
        return arr

    results = extract(obj, arr, key)
    return results

def ValidateResponse(record, response):
    if (record["Task"]) == "GET CURRENCY LIST":
        CONFIG.currencyList = response["currencies"]
        
    if CONFIG.currencyList == []:
        headers = {
            'x-rapidapi-host': CONFIG.API_Host,
            'x-rapidapi-key': CONFIG.API_Key
        }
        url = "https://currency-converter5.p.rapidapi.com/currency/list"
        querystring = {"format":"json"}
        response = requests.get(url, headers=headers, params=querystring)
        CONFIG.currencyList = json.loads(response.text)

    if (record["Task"]) == "CONVERT CURRENCY" or (record["Task"]) == "HISTORIC RATE CHECK":
        resAmount = extract_values(response, 'amount')[0]
        floatAmount = float(resAmount)
        if resAmount == record["Amount"]:
            CONFIG.logger1.info("Amount " + resAmount + " matched expected value")    
        else:
            CONFIG.logger1.info("Amount " + resAmount + " did not match expected value " + str(record['Amount']))
        
        resBaseCode = extract_values(response, 'base_currency_code')[0]
        if resBaseCode == record["FromCurrency"]:
            CONFIG.logger1.info("Base Currency Code " + resBaseCode + " matched expected value")    
        else:
            CONFIG.logger1.info("Base Currency Code " + resBaseCode + " did not match expected value " + record['FromCurrency']) 

        resBaseName = extract_values(response, 'base_currency_name')[0] 
        currencyName = CONFIG.currencyList[record['FromCurrency']]
        if resBaseName == currencyName:
            CONFIG.logger1.info("Base Currency Name " + resBaseName + " matched expected value")    
        else:
            CONFIG.logger1.info("Base Currency Name " + resBaseName + " did not match expected value " + currencyName) 
        
        nestedDict = response['rates']
        for resRateCode, value in nestedDict.items(): 
            if resRateCode == record["ToCurrency"]:
                CONFIG.logger1.info("Base Currency Code " + resRateCode + " matched expected value")    
            else:
                CONFIG.logger1.info("Base Currency Code " + resRateCode + " did not match expected value " + record['ToCurrency']) 
            
        resCurrencyName = extract_values(response, 'currency_name')[0] 
        currencyName = CONFIG.currencyList[record['ToCurrency']]
        if resCurrencyName == currencyName:
            CONFIG.logger1.info("Base Currency Name " + resCurrencyName + " matched expected value")    
        else:
            CONFIG.logger1.info("Base Currency Name " + resCurrencyName + " did not match expected value " + currencyName) 

        resRate = extract_values(response, 'rate')[0]
        floatRate = float(resRate)
        CONFIG.logger1.info("Currency rate returned was " + resRate)
        
        resRateForAmount = extract_values(response, 'rate_for_amount')[0]
        floatRateForAmount = float(resRateForAmount)
        floatCalculatedValue = floatAmount * floatRate
        if floatRateForAmount == floatCalculatedValue:
            CONFIG.logger1.info("Rate for Amount " + resRateForAmount + " matched expected value")    
        else:
            CONFIG.logger1.info("Rate for Amount " + resRateForAmount + " did not match expected value: " + resAmount + " * " + resRate) 

def ConvertCurrencyAPI(record):
    headers = {
        'x-rapidapi-host': CONFIG.API_Host,
        'x-rapidapi-key': CONFIG.API_Key
        }
    
    if (record["Task"]) == "GET CURRENCY LIST":  
        url = "https://currency-converter5.p.rapidapi.com/currency/list"
        querystring = {"format":"json"}
        #payload = "{'format':'json'}"
    elif (record["Task"]) == "CONVERT CURRENCY":  
        url = "https://currency-converter5.p.rapidapi.com/currency/convert"    
        querystring = {"format":"json","from":record["FromCurrency"],"to":record["ToCurrency"],"amount":record["Amount"]}
        #payload = "{'format':'json','from':{},'to':{},'amount':{}}".format(record["FromCurrency"], record["ToCurrency"], record["Amount"])
    elif (record["Task"]) == "HISTORIC RATE CHECK":  
        url = "https://currency-converter5.p.rapidapi.com/currency/historical/" + str(record["Date"])
        querystring = {"format":"json","from":record["FromCurrency"],"to":record["ToCurrency"],"amount":record["Amount"]}
        #payload = "{'format':'json','from':{},'to':{},'amount':{}}".format(record["FromCurrency"], record["ToCurrency"], record["Amount"])
        
    # approximate the requests module API call
    CONFIG.logger1.info("Request:\n\tcurl -X GET " + url + " " + str(querystring) + "\n")
        
    response = requests.get(url, headers=headers, params=querystring)
    json_data = json.loads(response.text)
    CONFIG.logger1.info("Response:\n\t" + str(json_data) + "\n")
    
    ValidateResponse(record, json_data)
    return