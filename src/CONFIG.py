import sys
import os
import logging
from logging import FileHandler
from datetime import datetime



filename = "../Data/CurrencyConverter.xlsm"
worksheet = worksheet = "Currency Converter"
LogFolder = "../Logs/"

API_Host = "currency-converter5.p.rapidapi.com"
API_Key = "9a08ed350cmshfd167ae20ffe912p184dc5jsne12451da4c5f"

errorList = []
currencyList = ""
verboseReporting = True
indent = "   "

#---------- LOG FILE SETUP ----------
resultsfile = worksheet + datetime.now().strftime("_%m%d_%H%M")
logfile = resultsfile + '.log'
imagefile = resultsfile + '.png' #move and change to include row number

#------- LOGGING SETUP -------
if not os.path.exists(LogFolder):
    os.makedirs(LogFolder)
        
logging.basicConfig(level=logging.INFO,
                    format='%(message)s',
                    datefmt='%m-%d %H:%M',
                    filename=LogFolder + logfile,
                    filemode='w')

console = logging.StreamHandler()
console.setLevel(logging.INFO)

formatter = logging.Formatter('%(message)s')
console.setFormatter(formatter)
logging.getLogger('').addHandler(console)
logger1 = logging.getLogger('Info')
logger2 = logging.getLogger('Error')

def CloseLogFile():
    logging.shutdown()
    return